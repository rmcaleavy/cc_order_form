class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me

  attr_accessible(:family_name, :store_name, :contact_name,
                  :phone, :address)

  validates(:family_name,  :presence => true)
  validates(:store_name,   :presence => false)
  validates(:contact_name, :presence => true)
  validates(:address,      :presence => true)
  validates(:email,        :presence => true, :uniqueness => true)

  has_many(:cards)
end

