class CardStyle < ActiveRecord::Base
  attr_accessible(:card_style)

  validates(:card_style,         :presence => true)

  has_many(:cards)

end
