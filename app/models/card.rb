class Card < ActiveRecord::Base
  attr_accessible(:quantity, :background,     :photo_borders,
                  :font, :text, :embellishment, :embellishment_color,
                  :credit, :corners)

  validates(:quantity,      :presence => true)


  #validates(:photos,        :presence => true)


  belongs_to(:user)
  belongs_to(:card_styles)
end
