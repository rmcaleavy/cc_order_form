class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.integer(:quantity)

      t.string(:photo_borders)
      t.string(:background)
      t.string(:font)
      t.text(:text)
      t.string(:embellishment)
      t.string(:embellishment_color)
      t.string(:credit)
      t.string(:corners)

      t.timestamps
    end
  end
end
