class CreateCardStyles < ActiveRecord::Migration
  def change
    create_table :card_styles do |t|
      t.string(:card_style, :null => false)
      t.timestamps
    end
  end
end
