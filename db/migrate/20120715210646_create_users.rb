class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string(:family_name,   :null => false)
      t.string(:store_name)
      t.string(:contact_name)
      t.string(:address,       :null => false)
      t.string(:email,         :null => false)
      t.string(:phone)
      t.string(:email,         :null => false)

      t.timestamps
    end
  end
end
