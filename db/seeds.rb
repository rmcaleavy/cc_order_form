# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create! do |user|
  user.family_name           = 'Foobar'
  user.store_name            = 'Store Name'
  user.contact_name          = 'Jodie Foster'
  user.address               = '1234 Street, Anytown, CO 80303'
  user.email                 = 'foo@bar.com'
  user.phone                 = '3036640673'
  user.password              = 'foobar'
  user.password_confirmation = 'foobar'
end


Card.create! do |card|
  card.quantity              = '100'

  card.photo_borders         = 'Gold'
  card.background            = 'Classic White'
  card.font                  = 'Avenir'
  card.text                  = 'This is my test!'
  card.embellishment         = 'Polka Dots'
  card.embellishment_color   = 'Gold'
  card.credit                = 'Mom & Pop'
  card.corners               = 'Round'
end

CardStyle.create!(card_style: 'Standard Flatcard - Horizontal')
CardStyle.create!(card_style: 'Standard Flatcard - Vertical')
CardStyle.create!(card_style: 'Standard Bookfold')
CardStyle.create!(card_style: 'Standard Tentfold')
CardStyle.create!(card_style: 'Standard Stretch Bookfold')
CardStyle.create!(card_style: 'Standard Stretch Tentfold')
CardStyle.create!(card_style: 'Standard Stretch Trifold Book')
CardStyle.create!(card_style: 'Standard Stretch Trifold Tent')

CardStyle.create!(card_style: 'Square Flatcard')
CardStyle.create!(card_style: 'Square Bookfold')
CardStyle.create!(card_style: 'Square Tentfold')
CardStyle.create!(card_style: 'Square Trifold Book')
CardStyle.create!(card_style: 'Square Trifold Tent')

CardStyle.create!(card_style: 'Slim Flatcard - Horizontal')
CardStyle.create!(card_style: 'Slim Flatcard - Vertical')

CardStyle.create!(card_style: 'Large Flatcard - Horizontal')
CardStyle.create!(card_style: 'Large Flatcard - Vertical')
CardStyle.create!(card_style: 'Large Bookfold')
CardStyle.create!(card_style: 'Large Tentfold')
CardStyle.create!(card_style: 'Large Stretch Bookfold')
CardStyle.create!(card_style: 'Large Stretch Tentfold')
CardStyle.create!(card_style: 'Large Trifold Book')
CardStyle.create!(card_style: 'Large Trifold Tent')

CardStyle.create!(card_style: 'Grand Flatcard - Horizontal')
CardStyle.create!(card_style: 'Grand Flatcard - Vertical')
CardStyle.create!(card_style: 'Grand Bookfold')
CardStyle.create!(card_style: 'Grand Tentfold')
CardStyle.create!(card_style: 'Grand Stretch Bookfold')
CardStyle.create!(card_style: 'Grand Stretch Tentfold')
CardStyle.create!(card_style: 'Grand Trifold Book')
CardStyle.create!(card_style: 'Grand Trifold Tent')




